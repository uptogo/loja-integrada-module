'use strict';

import { Expresso, Ecommerce } from 'uptogo-sdk';

const uptogoConfig = {
  apiKey: '5f1159760d4ee1933095627cf3d44878f33dbfb420369ada4c89e59ce645c0135c9685ce17320c7dd5389a0e2e30dbf080bb3edd02b9f7a9ca3a108a1eef0057',
  googleMapsApiKey: 'AIzaSyAkVz7ZsX-gF3gPgbWKBw1NQYq-0UUzsbU',
  cupomDesconto: null
};

const calcularUptogo = (cep, metodosEntrega, callback) => {
  let onErro = (erro, metodosEntrega, callback) => {    
    metodosEntrega.splice(metodosEntrega.findIndex(
      metodoEntrega => metodoEntrega.code === 'uptogo_expresso'
    ), 1);
    metodosEntrega.splice(metodosEntrega.findIndex(
      metodoEntrega => metodoEntrega.code === 'uptogo_ecommerce'
    ), 1);
    console.log(erro);
    callback(metodosEntrega);
  };

  (new Expresso())
  .setGoogleMapsApiKey(
    uptogoConfig.googleMapsApiKey,
    (erro, expresso) => {
      if (!erro) {
        expresso.setApiKey(
          uptogoConfig.apiKey,
          (erro, expresso) => {
            if (!erro) {
              expresso.setDestino(
                cep,
                (erro, expresso) => {
                  if (!erro) {
                    expresso.setCupomDesconto(
                      uptogoConfig.cupomDesconto
                    ).calcular(
                      (erro, expresso) => {
                        if (!erro) {
                          (new Ecommerce())
                          .setGoogleMapsApiKey(
                            uptogoConfig.googleMapsApiKey,
                            (erro, ecommerce) => {
                              if (!erro) {
                                ecommerce.setApiKey(
                                  uptogoConfig.apiKey,
                                  (erro, ecommerce) => {
                                    if (!erro) {
                                      ecommerce.setDestino(
                                        cep,
                                        (erro, ecommerce) => {
                                          if (!erro) {
                                            ecommerce.setCupomDesconto(
                                              uptogoConfig.cupomDesconto
                                            ).calcular(
                                              (erro, ecommerce) => {
                                                if (!erro) {
                                                  metodosEntrega.forEach(metodoEntrega => {
                                                    if (metodoEntrega.code === 'uptogo_expresso') {
                                                      metodoEntrega.name = 'UPtogo Expresso';
                                                      metodoEntrega.price = expresso.Valor;
                                                      metodoEntrega.deliveryTime = parseInt((expresso.Tempo / 60) / 24);
                                                    } else if (metodoEntrega.code === 'uptogo_ecommerce') {
                                                      metodoEntrega.name = 'UPtogo Ecommerce';
                                                      metodoEntrega.price = ecommerce.Valor;
                                                      metodoEntrega.deliveryTime = parseInt((ecommerce.Tempo / 60) / 24);
                                                    } else {
                                                      console.log('Verifique os identificadores dos métodos de entrega')
                                                    }
                                                  });
                                                  $.cookie("metodosEntrega", JSON.stringify(metodosEntrega));
                                                  callback(metodosEntrega);
                                                } else {
                                                  onErro(erro, metodosEntrega, callback);
                                                }
                                              }
                                            )
                                          } else {
                                            onErro(erro, metodosEntrega, callback);
                                          }
                                        }
                                      )
                                    } else {
                                      onErro(erro, metodosEntrega, callback);
                                    }
                                  }
                                )
                              } else {
                                onErro(erro, metodosEntrega, callback);
                              }
                            }
                          );
                        } else {
                          onErro(erro, metodosEntrega, callback);
                        }
                      }
                    )
                  } else {
                    onErro(erro, metodosEntrega, callback);
                  }
                }
              )
            } else {
              onErro(erro, metodosEntrega, callback);
            }
          }
        )
      } else {
        onErro(erro, metodosEntrega, callback);
      }
    }
  );
}

jQuery.getJSON = function(url, requestData, callback) {
  return jQuery.get(url, requestData, function(responseData, textStatus, jqXHR) {
    if (url.indexOf('/carrinho/frete') !== -1 || url.indexOf('/checkout/frete') !== -1) {
      calcularUptogo(requestData.cep, responseData, (metodosEntrega) => {
        callback(metodosEntrega, textStatus, jqXHR);
      });
    } else if (url.indexOf('/carrinho/endereco/adicionar') !== -1) {
      calcularUptogo(requestData.cep, responseData.deliveries, (metodosEntrega) => {
        responseData.deliveries = metodosEntrega;
        callback(responseData, textStatus, jqXHR);
      });
    } else if (url.indexOf('/carrinho/valor') !== -1) {
      JSON.parse($.cookie('metodosEntrega')).filter(metodo => 
        metodo.id === responseData.envio_selecionado
        &&
        (
          metodo.code === 'uptogo_expresso'
          ||
          metodo.code === 'uptogo_ecommerce'
        )
      ).forEach(metodo => {
        responseData.valor_frete = metodo.price;
        responseData.total += metodo.price;
      });
      callback(responseData, textStatus, jqXHR);
    } else {
      callback(responseData, textStatus, jqXHR);
    }
  } , "json");
}

if ($('#cart_selected_shipping_value').length) {
  var backupValidarCheckout = validarCheckout;
  validarCheckout = function() {
    let resultado = backupValidarCheckout();
    $('#cart_selected_shipping_value').val(0);
    return resultado;
  }
}